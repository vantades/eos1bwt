// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
`timescale 1ns/1ps
module bwt_control_s_axi
#(parameter
    C_S_AXI_ADDR_WIDTH = 11,
    C_S_AXI_DATA_WIDTH = 32
)(
    input  wire                          ACLK,
    input  wire                          ARESET,
    input  wire                          ACLK_EN,
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] AWADDR,
    input  wire                          AWVALID,
    output wire                          AWREADY,
    input  wire [C_S_AXI_DATA_WIDTH-1:0] WDATA,
    input  wire [C_S_AXI_DATA_WIDTH/8-1:0] WSTRB,
    input  wire                          WVALID,
    output wire                          WREADY,
    output wire [1:0]                    BRESP,
    output wire                          BVALID,
    input  wire                          BREADY,
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] ARADDR,
    input  wire                          ARVALID,
    output wire                          ARREADY,
    output wire [C_S_AXI_DATA_WIDTH-1:0] RDATA,
    output wire [1:0]                    RRESP,
    output wire                          RVALID,
    input  wire                          RREADY,
    output wire [31:0]                   len,
    input  wire [6:0]                    data_address0,
    input  wire                          data_ce0,
    output wire [31:0]                   data_q0,
    input  wire [6:0]                    transform_address0,
    input  wire                          transform_ce0,
    input  wire                          transform_we0,
    input  wire [31:0]                   transform_d0
);
//------------------------Address Info-------------------
// 0x000 : reserved
// 0x004 : reserved
// 0x008 : reserved
// 0x00c : reserved
// 0x010 : Data signal of len
//         bit 31~0 - len[31:0] (Read/Write)
// 0x014 : reserved
// 0x200 ~
// 0x3ff : Memory 'data' (128 * 32b)
//         Word n : bit [31:0] - data[n]
// 0x400 ~
// 0x5ff : Memory 'transform' (128 * 32b)
//         Word n : bit [31:0] - transform[n]
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

//------------------------Parameter----------------------
localparam
    ADDR_LEN_DATA_0     = 11'h010,
    ADDR_LEN_CTRL       = 11'h014,
    ADDR_DATA_BASE      = 11'h200,
    ADDR_DATA_HIGH      = 11'h3ff,
    ADDR_TRANSFORM_BASE = 11'h400,
    ADDR_TRANSFORM_HIGH = 11'h5ff,
    WRIDLE              = 2'd0,
    WRDATA              = 2'd1,
    WRRESP              = 2'd2,
    WRRESET             = 2'd3,
    RDIDLE              = 2'd0,
    RDDATA              = 2'd1,
    RDRESET             = 2'd2,
    ADDR_BITS                = 11;

//------------------------Local signal-------------------
    reg  [1:0]                    wstate = WRRESET;
    reg  [1:0]                    wnext;
    reg  [ADDR_BITS-1:0]          waddr;
    wire [C_S_AXI_DATA_WIDTH-1:0] wmask;
    wire                          aw_hs;
    wire                          w_hs;
    reg  [1:0]                    rstate = RDRESET;
    reg  [1:0]                    rnext;
    reg  [C_S_AXI_DATA_WIDTH-1:0] rdata;
    wire                          ar_hs;
    wire [ADDR_BITS-1:0]          raddr;
    // internal registers
    reg  [31:0]                   int_len = 'b0;
    // memory signals
    wire [6:0]                    int_data_address0;
    wire                          int_data_ce0;
    wire                          int_data_we0;
    wire [3:0]                    int_data_be0;
    wire [31:0]                   int_data_d0;
    wire [31:0]                   int_data_q0;
    wire [6:0]                    int_data_address1;
    wire                          int_data_ce1;
    wire                          int_data_we1;
    wire [3:0]                    int_data_be1;
    wire [31:0]                   int_data_d1;
    wire [31:0]                   int_data_q1;
    reg                           int_data_read;
    reg                           int_data_write;
    wire [6:0]                    int_transform_address0;
    wire                          int_transform_ce0;
    wire                          int_transform_we0;
    wire [3:0]                    int_transform_be0;
    wire [31:0]                   int_transform_d0;
    wire [31:0]                   int_transform_q0;
    wire [6:0]                    int_transform_address1;
    wire                          int_transform_ce1;
    wire                          int_transform_we1;
    wire [3:0]                    int_transform_be1;
    wire [31:0]                   int_transform_d1;
    wire [31:0]                   int_transform_q1;
    reg                           int_transform_read;
    reg                           int_transform_write;

//------------------------Instantiation------------------
// int_data
bwt_control_s_axi_ram #(
    .BYTES    ( 4 ),
    .DEPTH    ( 128 )
) int_data (
    .clk0     ( ACLK ),
    .address0 ( int_data_address0 ),
    .ce0      ( int_data_ce0 ),
    .we0      ( int_data_we0 ),
    .be0      ( int_data_be0 ),
    .d0       ( int_data_d0 ),
    .q0       ( int_data_q0 ),
    .clk1     ( ACLK ),
    .address1 ( int_data_address1 ),
    .ce1      ( int_data_ce1 ),
    .we1      ( int_data_we1 ),
    .be1      ( int_data_be1 ),
    .d1       ( int_data_d1 ),
    .q1       ( int_data_q1 )
);
// int_transform
bwt_control_s_axi_ram #(
    .BYTES    ( 4 ),
    .DEPTH    ( 128 )
) int_transform (
    .clk0     ( ACLK ),
    .address0 ( int_transform_address0 ),
    .ce0      ( int_transform_ce0 ),
    .we0      ( int_transform_we0 ),
    .be0      ( int_transform_be0 ),
    .d0       ( int_transform_d0 ),
    .q0       ( int_transform_q0 ),
    .clk1     ( ACLK ),
    .address1 ( int_transform_address1 ),
    .ce1      ( int_transform_ce1 ),
    .we1      ( int_transform_we1 ),
    .be1      ( int_transform_be1 ),
    .d1       ( int_transform_d1 ),
    .q1       ( int_transform_q1 )
);


//------------------------AXI write fsm------------------
assign AWREADY = (wstate == WRIDLE);
assign WREADY  = (wstate == WRDATA) && (!ar_hs);
assign BRESP   = 2'b00;  // OKAY
assign BVALID  = (wstate == WRRESP);
assign wmask   = { {8{WSTRB[3]}}, {8{WSTRB[2]}}, {8{WSTRB[1]}}, {8{WSTRB[0]}} };
assign aw_hs   = AWVALID & AWREADY;
assign w_hs    = WVALID & WREADY;

// wstate
always @(posedge ACLK) begin
    if (ARESET)
        wstate <= WRRESET;
    else if (ACLK_EN)
        wstate <= wnext;
end

// wnext
always @(*) begin
    case (wstate)
        WRIDLE:
            if (AWVALID)
                wnext = WRDATA;
            else
                wnext = WRIDLE;
        WRDATA:
            if (w_hs)
                wnext = WRRESP;
            else
                wnext = WRDATA;
        WRRESP:
            if (BREADY)
                wnext = WRIDLE;
            else
                wnext = WRRESP;
        default:
            wnext = WRIDLE;
    endcase
end

// waddr
always @(posedge ACLK) begin
    if (ACLK_EN) begin
        if (aw_hs)
            waddr <= AWADDR[ADDR_BITS-1:0];
    end
end

//------------------------AXI read fsm-------------------
assign ARREADY = (rstate == RDIDLE);
assign RDATA   = rdata;
assign RRESP   = 2'b00;  // OKAY
assign RVALID  = (rstate == RDDATA) & !int_data_read & !int_transform_read;
assign ar_hs   = ARVALID & ARREADY;
assign raddr   = ARADDR[ADDR_BITS-1:0];

// rstate
always @(posedge ACLK) begin
    if (ARESET)
        rstate <= RDRESET;
    else if (ACLK_EN)
        rstate <= rnext;
end

// rnext
always @(*) begin
    case (rstate)
        RDIDLE:
            if (ARVALID)
                rnext = RDDATA;
            else
                rnext = RDIDLE;
        RDDATA:
            if (RREADY & RVALID)
                rnext = RDIDLE;
            else
                rnext = RDDATA;
        default:
            rnext = RDIDLE;
    endcase
end

// rdata
always @(posedge ACLK) begin
    if (ACLK_EN) begin
        if (ar_hs) begin
            rdata <= 'b0;
            case (raddr)
                ADDR_LEN_DATA_0: begin
                    rdata <= int_len[31:0];
                end
            endcase
        end
        else if (int_data_read) begin
            rdata <= int_data_q1;
        end
        else if (int_transform_read) begin
            rdata <= int_transform_q1;
        end
    end
end


//------------------------Register logic-----------------
assign len = int_len;
// int_len[31:0]
always @(posedge ACLK) begin
    if (ARESET)
        int_len[31:0] <= 0;
    else if (ACLK_EN) begin
        if (w_hs && waddr == ADDR_LEN_DATA_0)
            int_len[31:0] <= (WDATA[31:0] & wmask) | (int_len[31:0] & ~wmask);
    end
end


//------------------------Memory logic-------------------
// data
assign int_data_address0      = data_address0;
assign int_data_ce0           = data_ce0;
assign int_data_we0           = 1'b0;
assign int_data_be0           = 1'b0;
assign int_data_d0            = 1'b0;
assign data_q0                = int_data_q0;
assign int_data_address1      = ar_hs? raddr[8:2] : waddr[8:2];
assign int_data_ce1           = ar_hs | (int_data_write & WVALID);
assign int_data_we1           = int_data_write & w_hs;
assign int_data_be1           = WSTRB;
assign int_data_d1            = WDATA;
// transform
assign int_transform_address0 = transform_address0;
assign int_transform_ce0      = transform_ce0;
assign int_transform_we0      = transform_we0;
assign int_transform_be0      = {4{transform_we0}};
assign int_transform_d0       = transform_d0;
assign int_transform_address1 = ar_hs? raddr[8:2] : waddr[8:2];
assign int_transform_ce1      = ar_hs | (int_transform_write & WVALID);
assign int_transform_we1      = int_transform_write & w_hs;
assign int_transform_be1      = WSTRB;
assign int_transform_d1       = WDATA;
// int_data_read
always @(posedge ACLK) begin
    if (ARESET)
        int_data_read <= 1'b0;
    else if (ACLK_EN) begin
        if (ar_hs && raddr >= ADDR_DATA_BASE && raddr <= ADDR_DATA_HIGH)
            int_data_read <= 1'b1;
        else
            int_data_read <= 1'b0;
    end
end

// int_data_write
always @(posedge ACLK) begin
    if (ARESET)
        int_data_write <= 1'b0;
    else if (ACLK_EN) begin
        if (aw_hs && AWADDR[ADDR_BITS-1:0] >= ADDR_DATA_BASE && AWADDR[ADDR_BITS-1:0] <= ADDR_DATA_HIGH)
            int_data_write <= 1'b1;
        else if (w_hs)
            int_data_write <= 1'b0;
    end
end

// int_transform_read
always @(posedge ACLK) begin
    if (ARESET)
        int_transform_read <= 1'b0;
    else if (ACLK_EN) begin
        if (ar_hs && raddr >= ADDR_TRANSFORM_BASE && raddr <= ADDR_TRANSFORM_HIGH)
            int_transform_read <= 1'b1;
        else
            int_transform_read <= 1'b0;
    end
end

// int_transform_write
always @(posedge ACLK) begin
    if (ARESET)
        int_transform_write <= 1'b0;
    else if (ACLK_EN) begin
        if (aw_hs && AWADDR[ADDR_BITS-1:0] >= ADDR_TRANSFORM_BASE && AWADDR[ADDR_BITS-1:0] <= ADDR_TRANSFORM_HIGH)
            int_transform_write <= 1'b1;
        else if (w_hs)
            int_transform_write <= 1'b0;
    end
end


endmodule


`timescale 1ns/1ps

module bwt_control_s_axi_ram
#(parameter
    BYTES  = 4,
    DEPTH  = 256,
    AWIDTH = log2(DEPTH)
) (
    input  wire               clk0,
    input  wire [AWIDTH-1:0]  address0,
    input  wire               ce0,
    input  wire               we0,
    input  wire [BYTES-1:0]   be0,
    input  wire [BYTES*8-1:0] d0,
    output reg  [BYTES*8-1:0] q0,
    input  wire               clk1,
    input  wire [AWIDTH-1:0]  address1,
    input  wire               ce1,
    input  wire               we1,
    input  wire [BYTES-1:0]   be1,
    input  wire [BYTES*8-1:0] d1,
    output reg  [BYTES*8-1:0] q1
);
//------------------------Local signal-------------------
reg  [BYTES*8-1:0] mem[0:DEPTH-1];
//------------------------Task and function--------------
function integer log2;
    input integer x;
    integer n, m;
begin
    n = 1;
    m = 2;
    while (m < x) begin
        n = n + 1;
        m = m * 2;
    end
    log2 = n;
end
endfunction
//------------------------Body---------------------------
// read port 0
always @(posedge clk0) begin
    if (ce0) q0 <= mem[address0];
end

// read port 1
always @(posedge clk1) begin
    if (ce1) q1 <= mem[address1];
end

genvar i;
generate
    for (i = 0; i < BYTES; i = i + 1) begin : gen_write
        // write port 0
        always @(posedge clk0) begin
            if (ce0 & we0 & be0[i]) begin
                mem[address0][8*i+7:8*i] <= d0[8*i+7:8*i];
            end
        end
        // write port 1
        always @(posedge clk1) begin
            if (ce1 & we1 & be1[i]) begin
                mem[address1][8*i+7:8*i] <= d1[8*i+7:8*i];
            end
        end
    end
endgenerate

endmodule

