// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xbwt.h"

extern XBwt_Config XBwt_ConfigTable[];

XBwt_Config *XBwt_LookupConfig(u16 DeviceId) {
	XBwt_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XBWT_NUM_INSTANCES; Index++) {
		if (XBwt_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XBwt_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XBwt_Initialize(XBwt *InstancePtr, u16 DeviceId) {
	XBwt_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XBwt_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XBwt_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

