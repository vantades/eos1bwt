// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XBWT_H
#define XBWT_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xbwt_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u32 Control_BaseAddress;
} XBwt_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u32 IsReady;
} XBwt;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XBwt_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XBwt_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XBwt_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XBwt_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XBwt_Initialize(XBwt *InstancePtr, u16 DeviceId);
XBwt_Config* XBwt_LookupConfig(u16 DeviceId);
int XBwt_CfgInitialize(XBwt *InstancePtr, XBwt_Config *ConfigPtr);
#else
int XBwt_Initialize(XBwt *InstancePtr, const char* InstanceName);
int XBwt_Release(XBwt *InstancePtr);
#endif


void XBwt_Set_len(XBwt *InstancePtr, u32 Data);
u32 XBwt_Get_len(XBwt *InstancePtr);
u32 XBwt_Get_data_BaseAddress(XBwt *InstancePtr);
u32 XBwt_Get_data_HighAddress(XBwt *InstancePtr);
u32 XBwt_Get_data_TotalBytes(XBwt *InstancePtr);
u32 XBwt_Get_data_BitWidth(XBwt *InstancePtr);
u32 XBwt_Get_data_Depth(XBwt *InstancePtr);
u32 XBwt_Write_data_Words(XBwt *InstancePtr, int offset, word_type *data, int length);
u32 XBwt_Read_data_Words(XBwt *InstancePtr, int offset, word_type *data, int length);
u32 XBwt_Write_data_Bytes(XBwt *InstancePtr, int offset, char *data, int length);
u32 XBwt_Read_data_Bytes(XBwt *InstancePtr, int offset, char *data, int length);
u32 XBwt_Get_transform_BaseAddress(XBwt *InstancePtr);
u32 XBwt_Get_transform_HighAddress(XBwt *InstancePtr);
u32 XBwt_Get_transform_TotalBytes(XBwt *InstancePtr);
u32 XBwt_Get_transform_BitWidth(XBwt *InstancePtr);
u32 XBwt_Get_transform_Depth(XBwt *InstancePtr);
u32 XBwt_Write_transform_Words(XBwt *InstancePtr, int offset, word_type *data, int length);
u32 XBwt_Read_transform_Words(XBwt *InstancePtr, int offset, word_type *data, int length);
u32 XBwt_Write_transform_Bytes(XBwt *InstancePtr, int offset, char *data, int length);
u32 XBwt_Read_transform_Bytes(XBwt *InstancePtr, int offset, char *data, int length);

#ifdef __cplusplus
}
#endif

#endif
