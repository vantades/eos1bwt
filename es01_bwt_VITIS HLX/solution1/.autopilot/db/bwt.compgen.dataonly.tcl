# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
len { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 16
	offset_end 23
}
data { 
	dir I
	width 32
	depth 128
	mode ap_memory
	offset 512
	offset_end 1023
}
transform { 
	dir O
	width 32
	depth 128
	mode ap_memory
	offset 1024
	offset_end 1535
}
}
dict set axilite_register_dict control $port_control


