// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// control
// 0x000 : reserved
// 0x004 : reserved
// 0x008 : reserved
// 0x00c : reserved
// 0x010 : Data signal of len
//         bit 31~0 - len[31:0] (Read/Write)
// 0x014 : reserved
// 0x200 ~
// 0x3ff : Memory 'data' (128 * 32b)
//         Word n : bit [31:0] - data[n]
// 0x400 ~
// 0x5ff : Memory 'transform' (128 * 32b)
//         Word n : bit [31:0] - transform[n]
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XBWT_CONTROL_ADDR_LEN_DATA       0x010
#define XBWT_CONTROL_BITS_LEN_DATA       32
#define XBWT_CONTROL_ADDR_DATA_BASE      0x200
#define XBWT_CONTROL_ADDR_DATA_HIGH      0x3ff
#define XBWT_CONTROL_WIDTH_DATA          32
#define XBWT_CONTROL_DEPTH_DATA          128
#define XBWT_CONTROL_ADDR_TRANSFORM_BASE 0x400
#define XBWT_CONTROL_ADDR_TRANSFORM_HIGH 0x5ff
#define XBWT_CONTROL_WIDTH_TRANSFORM     32
#define XBWT_CONTROL_DEPTH_TRANSFORM     128

