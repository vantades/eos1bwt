// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xbwt.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XBwt_CfgInitialize(XBwt *InstancePtr, XBwt_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XBwt_Set_len(XBwt *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XBwt_WriteReg(InstancePtr->Control_BaseAddress, XBWT_CONTROL_ADDR_LEN_DATA, Data);
}

u32 XBwt_Get_len(XBwt *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XBwt_ReadReg(InstancePtr->Control_BaseAddress, XBWT_CONTROL_ADDR_LEN_DATA);
    return Data;
}

u32 XBwt_Get_data_BaseAddress(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_BASE);
}

u32 XBwt_Get_data_HighAddress(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_HIGH);
}

u32 XBwt_Get_data_TotalBytes(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (XBWT_CONTROL_ADDR_DATA_HIGH - XBWT_CONTROL_ADDR_DATA_BASE + 1);
}

u32 XBwt_Get_data_BitWidth(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBWT_CONTROL_WIDTH_DATA;
}

u32 XBwt_Get_data_Depth(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBWT_CONTROL_DEPTH_DATA;
}

u32 XBwt_Write_data_Words(XBwt *InstancePtr, int offset, word_type *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XBWT_CONTROL_ADDR_DATA_HIGH - XBWT_CONTROL_ADDR_DATA_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(int *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_BASE + (offset + i)*4) = *(data + i);
    }
    return length;
}

u32 XBwt_Read_data_Words(XBwt *InstancePtr, int offset, word_type *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XBWT_CONTROL_ADDR_DATA_HIGH - XBWT_CONTROL_ADDR_DATA_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(int *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_BASE + (offset + i)*4);
    }
    return length;
}

u32 XBwt_Write_data_Bytes(XBwt *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XBWT_CONTROL_ADDR_DATA_HIGH - XBWT_CONTROL_ADDR_DATA_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(char *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_BASE + offset + i) = *(data + i);
    }
    return length;
}

u32 XBwt_Read_data_Bytes(XBwt *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XBWT_CONTROL_ADDR_DATA_HIGH - XBWT_CONTROL_ADDR_DATA_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(char *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_DATA_BASE + offset + i);
    }
    return length;
}

u32 XBwt_Get_transform_BaseAddress(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_BASE);
}

u32 XBwt_Get_transform_HighAddress(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_HIGH);
}

u32 XBwt_Get_transform_TotalBytes(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (XBWT_CONTROL_ADDR_TRANSFORM_HIGH - XBWT_CONTROL_ADDR_TRANSFORM_BASE + 1);
}

u32 XBwt_Get_transform_BitWidth(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBWT_CONTROL_WIDTH_TRANSFORM;
}

u32 XBwt_Get_transform_Depth(XBwt *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XBWT_CONTROL_DEPTH_TRANSFORM;
}

u32 XBwt_Write_transform_Words(XBwt *InstancePtr, int offset, word_type *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XBWT_CONTROL_ADDR_TRANSFORM_HIGH - XBWT_CONTROL_ADDR_TRANSFORM_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(int *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_BASE + (offset + i)*4) = *(data + i);
    }
    return length;
}

u32 XBwt_Read_transform_Words(XBwt *InstancePtr, int offset, word_type *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XBWT_CONTROL_ADDR_TRANSFORM_HIGH - XBWT_CONTROL_ADDR_TRANSFORM_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(int *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_BASE + (offset + i)*4);
    }
    return length;
}

u32 XBwt_Write_transform_Bytes(XBwt *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XBWT_CONTROL_ADDR_TRANSFORM_HIGH - XBWT_CONTROL_ADDR_TRANSFORM_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(char *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_BASE + offset + i) = *(data + i);
    }
    return length;
}

u32 XBwt_Read_transform_Bytes(XBwt *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XBWT_CONTROL_ADDR_TRANSFORM_HIGH - XBWT_CONTROL_ADDR_TRANSFORM_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(char *)(InstancePtr->Control_BaseAddress + XBWT_CONTROL_ADDR_TRANSFORM_BASE + offset + i);
    }
    return length;
}

