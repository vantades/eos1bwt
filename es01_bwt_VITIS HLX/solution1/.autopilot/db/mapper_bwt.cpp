#include <systemc>
#include <vector>
#include <iostream>
#include "hls_stream.h"
#include "ap_int.h"
#include "ap_fixed.h"
using namespace std;
using namespace sc_dt;
class AESL_RUNTIME_BC {
  public:
    AESL_RUNTIME_BC(const char* name) {
      file_token.open( name);
      if (!file_token.good()) {
        cout << "Failed to open tv file " << name << endl;
        exit (1);
      }
      file_token >> mName;//[[[runtime]]]
    }
    ~AESL_RUNTIME_BC() {
      file_token.close();
    }
    int read_size () {
      int size = 0;
      file_token >> mName;//[[transaction]]
      file_token >> mName;//transaction number
      file_token >> mName;//pop_size
      size = atoi(mName.c_str());
      file_token >> mName;//[[/transaction]]
      return size;
    }
  public:
    fstream file_token;
    string mName;
};
extern "C" void bwt(int, int*, int*);
extern "C" void apatb_bwt_hw(int __xlx_apatb_param_len, volatile void * __xlx_apatb_param_data, volatile void * __xlx_apatb_param_transform) {
  // Collect __xlx_data__tmp_vec
  vector<sc_bv<32> >__xlx_data__tmp_vec;
  for (int j = 0, e = 128; j != e; ++j) {
    __xlx_data__tmp_vec.push_back(((int*)__xlx_apatb_param_data)[j]);
  }
  int __xlx_size_param_data = 128;
  int __xlx_offset_param_data = 0;
  int __xlx_offset_byte_param_data = 0*4;
  int* __xlx_data__input_buffer= new int[__xlx_data__tmp_vec.size()];
  for (int i = 0; i < __xlx_data__tmp_vec.size(); ++i) {
    __xlx_data__input_buffer[i] = __xlx_data__tmp_vec[i].range(31, 0).to_uint64();
  }
  // Collect __xlx_transform__tmp_vec
  vector<sc_bv<32> >__xlx_transform__tmp_vec;
  for (int j = 0, e = 128; j != e; ++j) {
    __xlx_transform__tmp_vec.push_back(((int*)__xlx_apatb_param_transform)[j]);
  }
  int __xlx_size_param_transform = 128;
  int __xlx_offset_param_transform = 0;
  int __xlx_offset_byte_param_transform = 0*4;
  int* __xlx_transform__input_buffer= new int[__xlx_transform__tmp_vec.size()];
  for (int i = 0; i < __xlx_transform__tmp_vec.size(); ++i) {
    __xlx_transform__input_buffer[i] = __xlx_transform__tmp_vec[i].range(31, 0).to_uint64();
  }
  // DUT call
  bwt(__xlx_apatb_param_len, __xlx_data__input_buffer, __xlx_transform__input_buffer);
// print __xlx_apatb_param_data
  sc_bv<32>*__xlx_data_output_buffer = new sc_bv<32>[__xlx_size_param_data];
  for (int i = 0; i < __xlx_size_param_data; ++i) {
    __xlx_data_output_buffer[i] = __xlx_data__input_buffer[i+__xlx_offset_param_data];
  }
  for (int i = 0; i < __xlx_size_param_data; ++i) {
    ((int*)__xlx_apatb_param_data)[i] = __xlx_data_output_buffer[i].to_uint64();
  }
// print __xlx_apatb_param_transform
  sc_bv<32>*__xlx_transform_output_buffer = new sc_bv<32>[__xlx_size_param_transform];
  for (int i = 0; i < __xlx_size_param_transform; ++i) {
    __xlx_transform_output_buffer[i] = __xlx_transform__input_buffer[i+__xlx_offset_param_transform];
  }
  for (int i = 0; i < __xlx_size_param_transform; ++i) {
    ((int*)__xlx_apatb_param_transform)[i] = __xlx_transform_output_buffer[i].to_uint64();
  }
}
