#include <iostream>

void bwt (int len, unsigned int data[128], unsigned int (&transform)[128]) {
#pragma HLS INTERFACE s_axilite port=len
#pragma HLS INTERFACE s_axilite port=data
#pragma HLS INTERFACE s_axilite port=transform
#pragma HLS INTERFACE ap_ctrl_none port=return

	unsigned int table[128][128];
	unsigned int actual_string[128];

//	getting the strings
	actual_string[0] = 2;
	for (int i = 0; i < len; i++) {
		if (data[i] == 2 || data[i] == 3) {
			cout << "ERROR!!! Data cannot contain STX or EXT";
			}
		else {
			actual_string[i + 1] = data[i];
			}
	    }
	actual_string[len + 1] = 3;

//	copying string to rotation array
	for (int aa = 0; aa < len + 2; aa++) {
		table[len + 1][aa] = actual_string[aa];
	}

	//	rotation of string
	for (int ax = len; ax >= 0; ax--) {
		table[ax][0] = table[ax + 1][len + 1];
		for (int ay = len + 1; ay > 0; ay--) {
			table[ax][ay] = table[ax + 1][ay - 1];
			}
		}

//	sorting start
	   int ixe, jxe;
	    for (ixe = 0; ixe < len + 2-1; ixe++)

	    {
	    	for (jxe = 0; jxe < len + 2-ixe-1; jxe++) {
	    		if (table[jxe][0] > table[jxe+1][0])
	    		{
	    			int xxx;
	    			int axe[128];
	    			int ate[128];
	    			for (xxx = 0; xxx < len + 2; xxx++){
	    				axe[xxx] = table[jxe][xxx];
	    				ate[xxx] = table[jxe+1][xxx];
	    				table[jxe][xxx] = ate[xxx];
	    				table[jxe+1][xxx] = axe[xxx];
	                 	 }
	             }

	        else if (table[jxe][0] == table[jxe+1][0])
	             {
	            int fxe = 1;
	             int aaa =0;
	             while (table[jxe][fxe] == table[jxe+1][fxe] and aaa == 0)
               {
	            	 if (fxe == len + 2) { aaa = 1; }
	                     else { fxe++; }
	            }
	             if (aaa == 1) {

	               }
	               else if (table[jxe][fxe] > table[jxe+1][fxe])
	                {
	                  int yyy;
	                  int bxe[128];
	                  int bte[128];
	                  for (yyy = 0; yyy < len + 2; yyy++){
	                	  bxe[yyy] = table[jxe][yyy];
	                	  bte[yyy] = table[jxe+1][yyy];
	                	  table[jxe][yyy] = bte[yyy];
	                	  table[jxe+1][yyy] = bxe[yyy];
	                  }

	                }
	             }

	            	}

	    }
//sorting end

	for (int az = 0; az < len + 2; az++) {
			transform[az] = table[az][len + 1] ;
			}

}

